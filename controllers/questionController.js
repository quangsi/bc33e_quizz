let renderRadioButton = (data) => {
  return `<div class="form-check col-12">
    <label class="form-check-label">
    <input type="radio" class="form-check-input" name="singleChoice" id="" value=${data.exact} >
   ${data.content}
  </label>
</div>`;
};

let renderSingleChoice = (question) => {
  let contentHTML = `    
                  <p>${question.content}</p>
                    `;

  question.answers.forEach((answer) => {
    contentHTML += renderRadioButton(answer);
  });

  document.getElementById("quiz_content").innerHTML = contentHTML;
};

let renderFillInBlank = (question) => {
  let contentHTML = /*html*/ `    
                  <p>${question.content}</p>
                  <br />
                  <div class="form-group col-12">
                           <input   data-no-answer='${question.answers[0].content}'     type="text" class="form-control" id="fillToInput" />
                  </div>

                    `;

  document.getElementById("quiz_content").innerHTML = contentHTML;
};

export let renderQuestion = (question, currentQuestionIndex, total) => {
  // show stt câu hiện tại

  document.getElementById("current_step").innerText = `${
    currentQuestionIndex + 1
  }/${total}`;

  if (question.questionType == 1) {
    // gọi hàm render sigle choice
    renderSingleChoice(question);
  } else {
    // gọi hàm render fill in blank
    renderFillInBlank(question);
  }
};

let checkSingleChoice = () => {
  if (!document.querySelector('input[name="singleChoice"]:checked')) {
    return false;
  }
  let userAnswer = document.querySelector(
    'input[name="singleChoice"]:checked'
  ).value;
  if (userAnswer == "true") {
    return true;
  } else {
    return false;
  }
};

let checkFillInBlank = () => {
  let inputEl = document.getElementById("fillToInput");
  console.log("inputEl: ", inputEl.dataset);
  return inputEl.value == inputEl.dataset.noAnswer;
};

export let checkQuestion = (question) => {
  if (question.questionType == 1) {
    checkSingleChoice();
    // gọi hàm check single choice
  } else {
    checkFillInBlank();
    // gọi hàm check fill in blank
  }
};
