import { checkQuestion, renderQuestion } from "./questionController.js";
import { data_question } from "../data/questions.js";
let currentQuestionIndex = 0;

// lần đầu tiên render
renderQuestion(
  data_question[currentQuestionIndex],
  currentQuestionIndex,
  data_question.length
);

// khi user click
let handleNextQuestion = () => {
  checkQuestion(data_question[currentQuestionIndex]);

  currentQuestionIndex++;

  let currentQuestion = data_question[currentQuestionIndex];

  if (currentQuestionIndex >= data_question.length) {
    return;
  }
  renderQuestion(currentQuestion, currentQuestionIndex, data_question.length);
};

window.handleNextQuestion = handleNextQuestion;
